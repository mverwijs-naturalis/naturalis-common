package nl.naturalis.common.time;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalQuery;
import java.util.function.UnaryOperator;

import nl.naturalis.common.Check;

/**
 * A {@code ParseSpec} specifies how to parse a date string. The following can be specified when parsing a date string:
 * <ol>
 * <li>A {@code DateTimeFormatter} instance. <i>Required.</i>
 * <li>A {@link UnaryOperator} that transforms the input string before it is handed over to the {@code DateTimeFormatter}. <i>Optional</i>
 * (may be null). If specified and {@code UnaryOperator.apply} returns null, the date string will be treated as not parsable by this
 * {@code ParseSpec}.
 * <li>An array of {@code TemporalQuery} instances that specify the type of {@code java.time} objects that the date string should be parsed
 * into (for example {@link LocalDate#from(java.time.temporal.TemporalAccessor) LocalDate::from} or
 * {@link OffsetDateTime#from(java.time.temporal.TemporalAccessor) OffsetDateTime::from}). <i>Required.</i> If you specify just one
 * {@code TemporalQuery}, the formatter's {@link DateTimeFormatter#parse(CharSequence) parse} method is called, else its
 * {@link DateTimeFormatter#parseBest(CharSequence, TemporalQuery...) parseBest} method.
 * </ol>
 */
public final class ParseSpec {

  /**
   * A ready-made {@code ParseSpec} that parses date strings using the {@link DateTimeFormatter#ISO_INSTANT ISO_INSTANT} formatter into an
   * instance of {@link Instant}.
   */
  public static final ParseSpec ISO_INSTANT = new ParseSpec(DateTimeFormatter.ISO_INSTANT, Instant::from);
  /**
   * A ready-made {@code ParseSpec} that parses date strings using the {@link DateTimeFormatter#ISO_INSTANT ISO_LOCAL_DATE} formatter into
   * an instance of {@link LocalDate}.
   */
  public static final ParseSpec ISO_LOCAL_DATE = new ParseSpec(DateTimeFormatter.ISO_LOCAL_DATE, LocalDate::from);
  /**
   * A ready-made {@code ParseSpec} that parses date strings using the {@link DateTimeFormatter#ISO_INSTANT ISO_LOCAL_DATE_TIME} formatter
   * into an instance of {@link LocalDateTime}.
   */
  public static final ParseSpec ISO_LOCAL_DATE_TIME = new ParseSpec(DateTimeFormatter.ISO_LOCAL_DATE_TIME, LocalDateTime::from);

  final UnaryOperator<String> filter;
  final DateTimeFormatter formatter;
  final TemporalQuery<?>[] parseInto;

  private final String pattern;

  public ParseSpec(String pattern, TemporalQuery<?>... parseInto) {
    this(null, pattern, parseInto);
  }

  public ParseSpec(DateTimeFormatter formatter, TemporalQuery<?>... parseInto) {
    this(null, formatter, parseInto);
  }

  public ParseSpec(UnaryOperator<String> filter, String pattern, TemporalQuery<?>... parseInto) {
    this.filter = filter;
    this.pattern = Check.notNull(pattern, "pattern");
    this.formatter = DateTimeFormatter.ofPattern(pattern);
    this.parseInto = Check.notNull(parseInto, "parseInto");
  }

  public ParseSpec(UnaryOperator<String> filter, DateTimeFormatter formatter, TemporalQuery<?>... parseInto) {
    this.filter = filter;
    this.pattern = null;
    this.formatter = Check.notNull(formatter, "formatter");
    this.parseInto = Check.notNull(parseInto, "parseInto");
  }

  /**
   * Returns the filter used to transform the input string, or null if no filter is used by this {@code ParseSpec}. The filter's
   * {@link UnaryOperator#apply(Object) apply} method is explicitly allowed to return null, in which case the input string will be treated
   * as not parsable by this {@code ParseSpec}.
   * 
   * @return
   */
  public UnaryOperator<String> getFilter() {
    return filter;
  }

  /**
   * Returns the {@link DateTimeFormatter} used to parse the date string.
   * 
   * @return
   */
  public DateTimeFormatter getFormatter() {
    return formatter;
  }

  /**
   * Returns the type of date/time object(s) the date string is allowed to be parsed into.
   * 
   * @return
   */
  public TemporalQuery<?>[] getParseInto() {
    return parseInto;
  }

  /**
   * Returns the date/time pattern this {@code ParseSpec} was instantiated with, or null if the {@code ParseSpec} was instantiated with a
   * ready-made {@link DateTimeFormatter} object.
   * 
   * @return
   */
  public String getPattern() {
    return pattern;
  }

}
