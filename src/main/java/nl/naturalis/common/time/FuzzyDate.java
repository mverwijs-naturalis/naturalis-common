package nl.naturalis.common.time;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.Year;
import java.time.YearMonth;
import java.time.ZoneId;
import java.time.temporal.TemporalAccessor;

import static java.time.temporal.ChronoField.DAY_OF_MONTH;
import static java.time.temporal.ChronoField.HOUR_OF_DAY;
import static java.time.temporal.ChronoField.MINUTE_OF_HOUR;
import static java.time.temporal.ChronoField.MONTH_OF_YEAR;
import static java.time.temporal.ChronoField.SECOND_OF_MINUTE;
import static java.time.temporal.ChronoField.YEAR;

/**
 * A {@code FuzzyDate} represents the result of parsing a date string using the {@link FuzzyDateParser}. If the {@code FuzzyDateParser} was
 * able to parse the date string at all, it will produce a {@code FuzzyDate} where at least the year is known. The {@code FuzzyDate} in turn
 * lets you retrieve regular {@code java.time} objects from it. You cannot instantiate a {@code FuzzyDate}; you obtain an instance by
 * calling {@link FuzzyDateParser#parse(String) parse} on the {@code FuzzyDateParser}.
 */
public final class FuzzyDate {

  private static final ZoneId UTC = ZoneId.of("Z");

  private final TemporalAccessor ta;
  private final String verbatim;
  private final ParseSpec parseSpec;

  // Can only be instantiated by FuzzyDateParser
  FuzzyDate(TemporalAccessor ta, String verbatim, ParseSpec parseSpec) {
    this.ta = ta;
    this.verbatim = verbatim;
    this.parseSpec = parseSpec;
  }

  /**
   * Converts this {@code FuzzyDate} to an {@link Instant java.time.Instant} object. } catch(DateTimeException e) { throw new
   * FuzzyDateException("Unable to convert FuzzyDate to java.time.Instant: " + e.getMessage()); }
   * 
   * @return
   */
  public Instant toInstant() {
    if (ta.getClass() == Instant.class) {
      return (Instant) ta;
    } else if (ta.getClass() == LocalDate.class) {
      return ((LocalDate) ta).atStartOfDay(UTC).toInstant();
    } else {
      return Instant.from(ta);
    }
  }

  /**
   * Converts this {@code FuzzyDate} to a {@link LocalDateTime} object, setting month and day to 1 if unknown, and setting hour, minute and
   * second to 0 if unknown.
   * 
   * @return
   */
  public LocalDateTime toLocalDateTime() {
    if (ta.getClass() == LocalDateTime.class) {
      return (LocalDateTime) ta;
    } else if (ta.getClass() == OffsetDateTime.class) {
      return ((OffsetDateTime) ta).toLocalDateTime();
    } else if (ta.getClass() == LocalDate.class) {
      return ((LocalDate) ta).atStartOfDay();
    } else if (ta.getClass() == Instant.class) {
      return LocalDateTime.ofInstant((Instant) ta, UTC);
    } else {
      int year = ta.get(YEAR);
      int month = ta.isSupported(MONTH_OF_YEAR) ? ta.get(MONTH_OF_YEAR) : 1;
      int day = ta.isSupported(DAY_OF_MONTH) ? ta.get(DAY_OF_MONTH) : 1;
      int hour = ta.isSupported(HOUR_OF_DAY) ? ta.get(HOUR_OF_DAY) : 0;
      int minute = ta.isSupported(MINUTE_OF_HOUR) ? ta.get(MINUTE_OF_HOUR) : 0;
      int second = ta.isSupported(SECOND_OF_MINUTE) ? ta.get(SECOND_OF_MINUTE) : 0;
      return LocalDateTime.of(year, month, day, hour, minute, second);
    }
  }

  /**
   * Converts this {@code FuzzyDate} to a {@link LocalDate} object, setting month and/or day to 1 if unknown.
   *
   * @return
   */
  public LocalDate toLocalDate() {
    if (ta.getClass() == LocalDate.class) {
      return (LocalDate) ta;
    } else if (ta.getClass() == OffsetDateTime.class) {
      return ((OffsetDateTime) ta).toLocalDate();
    } else if (ta.getClass() == LocalDateTime.class) {
      return ((LocalDateTime) ta).toLocalDate();
    } else if (ta.getClass() == Instant.class) {
      return LocalDate.ofInstant((Instant) ta, ZoneId.of("Z"));
    } else {
      int year = ta.get(YEAR);
      int month = ta.isSupported(MONTH_OF_YEAR) ? ta.get(MONTH_OF_YEAR) : 1;
      int day = ta.isSupported(DAY_OF_MONTH) ? ta.get(DAY_OF_MONTH) : 1;
      return LocalDate.of(year, month, day);
    }
  }

  /**
   * Returns the most granular date/time object that could be parsed out of the date string.
   * 
   * @return
   */
  public TemporalAccessor bestMatch() {
    int year = ta.get(YEAR);
    if (ta.isSupported(MONTH_OF_YEAR)) {
      int month = ta.get(MONTH_OF_YEAR);
      if (ta.isSupported(DAY_OF_MONTH)) {
        int day = ta.get(DAY_OF_MONTH);
        if (ta.isSupported(HOUR_OF_DAY)) {
          int hour = ta.get(HOUR_OF_DAY);
          int minute = ta.isSupported(MINUTE_OF_HOUR) ? ta.get(MINUTE_OF_HOUR) : 0;
          int second = ta.isSupported(SECOND_OF_MINUTE) ? ta.get(SECOND_OF_MINUTE) : 0;
          return LocalDateTime.of(year, month, day, hour, minute, second);
        }
        return LocalDate.of(year, month, day);
      }
      return YearMonth.of(year, month);
    }
    return Year.of(year);
  }

  /**
   * Returns the raw {@link TemporalAccessor} object that was created from the verbatim date string.
   * 
   * @return
   */
  public TemporalAccessor getTemporalAccessor() {
    return ta;
  }

  /**
   * Returns the original date string from which this FuzzyDate instance was created.
   *
   * @return
   */
  public String getVerbatim() {
    return verbatim;
  }

  /**
   * Returns the {@link ParseSpec} object that was used to parse the date string into a {@code FuzzyDate} instance.
   * 
   * @return
   */
  public ParseSpec getParseSpec() {
    return parseSpec;
  }

  /**
   * Returns true if month or day are unknown, false if both are known.
   *
   * @return
   */
  public boolean isFuzzyDate() {
    return !(ta.isSupported(MONTH_OF_YEAR)
        && ta.isSupported(DAY_OF_MONTH));
  }

  /**
   * Returns true if hour or minute are unknown, false if both are known. Seconds are ignored!
   *
   * @return
   */
  public boolean isFuzzyTime() {
    return !(ta.isSupported(HOUR_OF_DAY)
        && ta.isSupported(MINUTE_OF_HOUR));
  }

  /**
   * Equivalent to {@code isFuzzyDate() || isFuzzyTime()}.
   * 
   * @return
   */
  public boolean isFuzzyDateTime() {
    return isFuzzyDate() || isFuzzyTime();
  }

}
