package nl.naturalis.common;

/**
 * Methods for inspecting types.
 *
 * @author Ayco Holleman
 */
public class ClassMethods {

  private ClassMethods() {}

  /**
   * Tests whether {@code what} is a subclass or implementation of {@code superOrInterface}. Useful if you keep forgetting who should be the
   * caller and who the callee with the {@code Class.isAssignableFrom} method.
   * 
   * @param what
   * @param superOrInterface
   * @return
   */
  public static boolean isA(Class<?> what, Class<?> superOrInterface) {
    return superOrInterface.isAssignableFrom(what);
  }

  /**
   * Tests whether the provided object is an instance of {@code superOrInterface}.
   * 
   * @param obj
   * @param superOrInterface
   * @return
   */
  public static boolean isA(Object obj, Class<?> superOrInterface) {
    return superOrInterface.isAssignableFrom(obj.getClass());
  }

}
