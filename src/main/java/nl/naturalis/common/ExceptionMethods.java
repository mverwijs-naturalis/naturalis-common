package nl.naturalis.common;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;

import nl.naturalis.common.exception.ExceptionSource;
import nl.naturalis.common.exception.UncheckedException;

/**
 * Methods related to exception handling.
 * 
 * @author Ayco Holleman
 *
 */
public class ExceptionMethods {

  private ExceptionMethods() {}

  /**
   * Returns the root cause of the specified throwable, or the throwable itself if it has no cause.
   * 
   * @param t
   * @return
   */
  public static Throwable getRootCause(Throwable t) {
    while (t.getCause() != null) {
      t = t.getCause();
    }
    return t;
  }

  /**
   * Returns the stack trace of the root cause of {@code t} as a string.
   * 
   * @param t
   * @return
   */
  public static String getRootStackTraceAsString(Throwable t) {
    ByteArrayOutputStream baos = new ByteArrayOutputStream(2048);
    getRootCause(t).printStackTrace(new PrintStream(baos));
    return baos.toString(StandardCharsets.UTF_8);
  }

  /**
   * Returns a detailed exception message that includes the class, method and line at which the exception occurred. Equivalent to
   * {@code new ExceptionSource(getRootCause(t)).getDetailedMessage()}.
   * 
   * @see ExceptionSource#getDetailedMessage()
   * 
   * @param t
   * @return
   */
  public static String getDetailedMessage(Throwable t) {
    return new ExceptionSource(getRootCause(t)).getDetailedMessage();
  }

  /**
   * Returns the provided throwable if it already is a {@link RuntimeException}, else a {@code RuntimeException} wrapping the throwable.
   * 
   * @param t A checked or unchecked exception
   * @return The provided throwable or a {@code RuntimeException} wrapping it
   */
  public static RuntimeException wrap(Throwable t) {
    if (t instanceof RuntimeException) {
      return (RuntimeException) t;
    }
    return new RuntimeException(t);
  }

  /**
   * Returns the provided throwable if it already is a {@link RuntimeException}, else an {@link UncheckedException} wrapping the throwable.
   * 
   * @param t A checked or unchecked exception
   * @return The provided throwable or an {@code UncheckedException} wrapping it
   */
  public static RuntimeException uncheck(Throwable t) {
    if (t instanceof RuntimeException) {
      return (RuntimeException) t;
    }
    return new UncheckedException(t);
  }

}
