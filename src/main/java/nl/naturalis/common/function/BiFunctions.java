package nl.naturalis.common.function;

import java.util.function.BiFunction;

/**
 * A dictionary of methods that can be used in places where a {@link BiFunction} is required. The methods do not necessarily provide new
 * functionality. Generally they just use existing classes and methods (in naturalis-common or elsewhere) to implement a {@code BiFunction}.
 */
public class BiFunctions {

  private BiFunctions() {}

}
