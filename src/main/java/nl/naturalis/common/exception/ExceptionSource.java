package nl.naturalis.common.exception;

import nl.naturalis.common.ExceptionMethods;

import static nl.naturalis.common.ArrayMethods.isEmpty;
import static nl.naturalis.common.ArrayMethods.notEmpty;

/**
 * An {@code ExceptionSource} provides detailed information about an error condition by homing in on a execution point along its stack
 * trace. By default it takes the first element of the stack trace, which represents the absolute origin if the exception, but only if you
 * happen to be dealing with the "root cause" exception. So this is probably only useful in combination with the functionality of
 * {@link ExceptionMethods#getRootCause(Throwable) ExceptionMethods.getRootCause} or {@link RootException}. Alternatively, you can let the
 * {@code ExceptionSource} instance search the stack trace for a particular execution point. The search targets the (fully-qualified) class
 * names in the stack trace (using a simple {@link String#contains(CharSequence) String.contains}). This enables you to figure out where
 * <i>in your own code</i> things went wrong, which often is what you are most interested in. For example:
 * 
 * <pre>
 * try {
 *   // stuff
 * } catch (Exception e) {
 *   ExceptionSource es = new ExceptionSource(e, "nl.naturalis");
 *   logger.error(es.getDetailedMessage());
 * }
 * </pre>
 */
public class ExceptionSource {

  private final Throwable exc;
  private final String search;
  private final StackTraceElement ste;

  /**
   * Equivalent to {@code new ExceptionSource(t, null)}.
   * 
   * @param t
   */
  public ExceptionSource(Throwable t) {
    this(t, null);
  }

  /**
   * Creates a new {@code ExceptionSource} for the provided exception, searching its stack trace for an execution point matching
   * {@code search} (using {@link String#contains(CharSequence) String.contains}). The {@code search} argument is explicitly allowed to be
   * null, in which case the first element of the stack trace is used to provide extra information about the exception.
   * 
   * @param t The exception to inspect
   * @param search The string to search for in the stack trace.
   */
  public ExceptionSource(Throwable t, String search) {
    this.exc = t;
    this.search = search;
    if (isEmpty(t.getStackTrace())) {
      this.ste = null;
    } else if (search == null) {
      this.ste = t.getStackTrace()[0];
    } else {
      this.ste = findOrigin(t, search);
    }
  }

  /**
   * Provides a detailed exception message that includes the class, method and line at which the exception occurred, or at the execution
   * point matching the search.
   * 
   * @return
   */
  public String getDetailedMessage() {
    StringBuilder sb = new StringBuilder(100);
    sb.append(exc.getClass().getName());
    if (ste == null) {
      if (search != null) {
        sb.append(" (not emanating from \"").append(search).append("\")");
      } else {
        sb.append(" (no stack trace available)");
      }
    } else {
      sb.append(" at ")
          .append(getClassName())
          .append('.')
          .append(getMethod())
          .append(" (line ")
          .append(getLine())
          .append(')');
    }
    if (exc.getMessage() != null) {
      sb.append(" - ").append(exc.getMessage());
    }
    return sb.toString();
  }

  /**
   * Returns {@link #getDetailedMessage() getDetailedMessage}.
   */
  public String toString() {
    return getDetailedMessage();
  }

  /**
   * Returns the exception wrapped by this {@code ExceptionSource}.
   */
  public Throwable getException() {
    return exc;
  }

  /**
   * Returns the execution point that was inspected.
   * 
   * @return
   */
  public StackTraceElement geStackTraceElement() {
    return ste;
  }

  /**
   * Returns the module in which the exception occurred or null if the exception came without a stack trace.
   * 
   * @return
   */
  public String getModule() {
    return ste == null ? null : ste.getModuleName();
  }

  /**
   * Returns the class in which the exception occurred or null if the exception came without a stack trace.
   * 
   * @return
   */
  public String getClassName() {
    return ste == null ? null : ste.getClassName();
  }

  /**
   * Returns the method in which the exception occurred or null if the exception came without a stack trace.
   * 
   * @return
   */
  public String getMethod() {
    return ste == null ? null : ste.getMethodName();
  }

  /**
   * Returns the line at which the exception occurred or -1 if the exception came without a stack trace.
   * 
   * @return
   */

  public int getLine() {
    return ste == null ? -1 : ste.getLineNumber();
  }

  private static StackTraceElement findOrigin(Throwable exc, String search) {
    StackTraceElement e = null;
    LOOP: for (Throwable t = exc; t != null; t = t.getCause()) {
      StackTraceElement[] trace = exc.getStackTrace();
      if (notEmpty(trace)) {
        for (StackTraceElement e0 : trace) {
          if (e0.getClassName().contains(search)) {
            e = e0;
            break LOOP;
          }
        }
      }
    }
    return e;
  }

}
