package nl.naturalis.common;

import java.util.Collection;
import java.util.function.Supplier;

/**
 * Methods for checking preconditions.
 *
 * @author Ayco Holleman
 */
public class Check {

  private Check() {}

  /**
   * Generic check method that can also be used for other purposes than checking preconditions. Throws the exception supplied by the
   * provided supplier if the provided condition evaluates to false.
   * 
   * @param <T> The type of exception thrown if the condition fails
   * @param condition The condition to evaluate
   * @param exceptionSupplier The exception supplier
   * @throws T The exception thrown if the condition fails
   */
  public static <T extends Exception> void that(boolean condition, Supplier<T> exceptionSupplier) throws T {
    if (!condition) {
      throw exceptionSupplier.get();
    }
  }

  /**
   * Generic check method that can also be used for other purposes than checking preconditions. Throws the exception supplied by the
   * provided exception supplier if the provided condition evaluates to false, else returns the result supplied by the result supplier.
   * 
   * @param <T> The type of exception thrown if the condition fails
   * @param <U> The type of object returned if the condition is met
   * @param condition The condition to evaluate
   * @param resultSupplier The result supplier
   * @param exceptionSupplier The exception supplier
   * @return The object supplied by the resultSupplier
   * @throws T The exception thrown if the condition fails
   */
  public static <T extends Exception, U> U that(boolean condition, Supplier<U> resultSupplier, Supplier<T> exceptionSupplier) throws T {
    if (condition) {
      return resultSupplier.get();
    }
    throw exceptionSupplier.get();
  }

  /**
   * Does nothing if the provided condition evaluates to {@code true}, else throws an {@link IllegalArgumentException} with the provided
   * message.
   * 
   * @param condition The condition to be evaluated
   * @param msg The exception message
   * @throws IllegalArgumentException If the condition evaluates to false
   */
  public static void argument(boolean condition, String msg) {
    if (!condition) {
      throw new IllegalArgumentException(msg);
    }
  }

  /**
   * Does nothing if the provided condition evaluates to {@code true}, else throws an {@link IllegalArgumentException} with the provided
   * message and message argument.
   * 
   * @param condition The condition to be evaluated
   * @param msg The exception message
   * @param msgArg The {@code String.format} message argument
   * @throws IllegalArgumentException If the condition evaluates to false
   */
  public static void argument(boolean condition, String msg, Object msgArg) {
    if (!condition) {
      throw new IllegalArgumentException(String.format(msg, new Object[] {msgArg}));
    }
  }

  /**
   * Does nothing if the provided condition evaluates to {@code true}, else throws an {@link IllegalArgumentException} with the provided
   * message and message arguments.
   * 
   * @param condition The condition to be evaluated
   * @param msg The exception message
   * @param msgArg0 The 1st {@code String.format} message argument
   * @param msgArg1 The 2nd {@code String.format} message argument
   * @param moreMsgArgs The remaining {@code String.format} message arguments
   * @throws IllegalArgumentException If the condition evaluates to false
   */
  public static void argument(boolean condition, String msg, Object msgArg0, Object msgArg1, Object... moreMsgArgs) {
    if (!condition) {
      Object[] args = ArrayMethods.prefix(moreMsgArgs, msgArg0, msgArg1);
      throw new IllegalArgumentException(String.format(msg, args));
    }
  }

  /**
   * Returns {@code arg} if it is not null, else throws an {@link IllegalArgumentException} with the message: "Illegal argument: null".
   * 
   * @param <T> The type of the argument being checked and returned
   * @param arg The argument being checked and returned
   * @return The argument
   * @throws IllegalArgumentException If the argument is null
   */
  public static <T> T notNull(T arg) {
    if (arg == null) {
      throw new IllegalArgumentException("Illegal argument: null");
    }
    return arg;
  }

  /**
   * Returns {@code arg} if it is not null, else throws an {@link IllegalArgumentException} with the message: "&lt;argName&gt; must not be
   * null". In other words, the 2nd argument is not supposed to be a complete message but just the name of the argument whose value is
   * tested.
   * 
   * @param <T> The type of the argument being checked and returned
   * @param arg The argument being checked and returned
   * @param argName The name of the argument being checked and returned
   * @return The argument
   * @throws IllegalArgumentException If the argument is null
   */
  public static <T> T notNull(T arg, String argName) {
    if (arg == null) {
      throw new IllegalArgumentException(argName + " must not be null");
    }
    return arg;
  }

  /**
   * Returns {@code arg} if it is not null, else throws an {@link IllegalArgumentException} with the provided message and message argument.
   * The {@code msgArg} argument is explicitly allowed to be null, in which case it is assumed that the provided message takes no message
   * arguments.
   * 
   * @param <T> The type of the argument being checked and returned
   * @param arg The argument being checked and returned
   * @param msg The exception message
   * @param msgArg The {@code String.format} message argument
   * @return The argument
   * @throws IllegalArgumentException If the argument is null
   */
  public static <T> T notNull(T arg, String msg, Object msgArg) {
    if (arg == null) {
      if (msgArg == null) {
        throw new IllegalArgumentException(msg);
      }
      throw new IllegalArgumentException(String.format(msg, new Object[] {msgArg}));
    }
    return arg;
  }

  /**
   * Returns {@code arg} if it is not null, else throws an {@link IllegalArgumentException} with the provided message and message arguments.
   * 
   * @param <T> The type of the argument being checked and returned
   * @param arg The argument being checked and returned
   * @param msg The exception message
   * @param msgArg0 The 1st {@code String.format} message argument
   * @param msgArg1 The 2nd {@code String.format} message argument
   * @param moreMsgArgs The remaining {@code String.format} message arguments
   * @return The argument
   * @throws IllegalArgumentException If the argument is null
   */
  public static <T> T notNull(T arg, String msg, Object msgArg0, Object msgArg1, Object... moreMsgArgs) {
    if (arg == null) {
      Object[] msgArgs = ArrayMethods.prefix(moreMsgArgs, msgArg0, msgArg1);
      throw new IllegalArgumentException(String.format(msg, msgArgs));
    }
    return arg;
  }

  /**
   * Returns the provided array if it is not null and none of its elements are null, else throws an {@link IllegalArgumentException}.
   * 
   * @param <T>
   * @param array
   * @param argName
   * @return
   */
  public static <T> T[] noneNull(T[] array, String argName) {
    for (T t : notNull(array, argName)) {
      if (t == null) {
        throw new IllegalArgumentException(argName + " must not contain null elements");
      }
    }
    return array;
  }

  /**
   * Returns the provided collection if it is not null and none of its elements are null, else throws an {@link IllegalArgumentException}.
   * 
   * @param <E>
   * @param <T>
   * @param collection
   * @param argName
   * @return
   */
  public static <E, T extends Collection<E>> T noneNull(T collection, String argName) {
    for (E e : notNull(collection, argName)) {
      if (e == null) {
        throw new IllegalArgumentException(argName + " must not contain null elements");
      }
    }
    return collection;
  }

  /**
   * Does nothing if the provided condition evaluates to {@code true}, else throws an {@link IllegalStateException} with the provided
   * message.
   * 
   * @param condition The condition to be evaluated
   * @param msg The exception message
   * @throws IllegalStateException If the condition evaluates to {@code false}
   */
  public static void state(boolean condition, String msg) {
    if (!condition) {
      throw new IllegalStateException(msg);
    }
  }

  /**
   * Does nothing if the provided condition evaluates to {@code true}, else throws an {@link IllegalStateException} with the provided
   * message and message argument.
   * 
   * @param condition The condition to be evaluated
   * @param msg The exception message
   * @param msgArg The {@code String.format} message argument
   * @throws IllegalStateException If the condition evaluates to {@code false}
   */
  public static void state(boolean condition, String msg, Object msgArg) {
    if (!condition) {
      throw new IllegalStateException(String.format(msg, new Object[] {msgArg}));
    }
  }

  /**
   * Does nothing if the provided condition evaluates to {@code true}, else throws an {@link IllegalStateException} with the provided
   * message and message arguments.
   * 
   * @param condition
   * @param msg The exception message
   * @param msgArg0 The 1st {@code String.format} message argument
   * @param msgArg1 The 2nd {@code String.format} message argument
   * @param moreMsgArgs The remaining {@code String.format} message arguments
   * @throws IllegalStateException If the condition evaluates to {@code false}
   */
  public static void state(boolean condition, String msg, Object msgArg0, Object msgArg1, Object... moreMsgArgs) {
    if (!condition) {
      Object[] args = ArrayMethods.prefix(moreMsgArgs, msgArg0, msgArg1);
      throw new IllegalStateException(String.format(msg, args));
    }
  }

}
