package nl.naturalis.common;

/**
 * General methods applicable to objects of any type.
 *
 * @author Ayco Holleman
 */
public class ObjectMethods {

  private ObjectMethods() {}

  /**
   * Returns the 1st argument if it is not null, else the 2nd argument.
   * 
   * @param <T>
   * @param subject
   * @param dfault
   * @return
   */
  public static <T> T ifNull(T subject, T dfault) {
    return subject == null ? dfault : subject;
  }

}
