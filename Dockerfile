FROM maven:3.6.3-jdk-11
COPY target/naturalis-common-2.0.1.jar /tmp/
RUN mvn org.apache.maven.plugins:maven-install-plugin:2.5.2:install-file -Dfile=/tmp/naturalis-common-2.0.1.jar
